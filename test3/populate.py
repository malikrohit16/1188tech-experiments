import os,sys
os.environ.setdefault('DJANGO_SETTINGS_MODULE','test3.settings')
import django 
django.setup()

from person.models import OrderSale
from faker import Faker 
from faker_schema.faker_schema import FakerSchema
from faker_schema.schema_loader import load_json_from_file, load_json_from_string

json_string = '{"order_id": "uuid4", "order_name": "name", "order_address":"address","email_address": "email"}'
                 


schema = load_json_from_string(json_string)
faker = FakerSchema()
fakegen = Faker()
data = faker.generate_fake(schema)

def populate(n = 10):

    for i in range(n):
        fake_name = fakegen.name()
        fake_invoiceNumber = fakegen.msisdn()
        fake_invoiceDate = fakegen.date_time(tzinfo=None, end_datetime=None)
        fake_shipmentDate = fakegen.date(pattern="%Y-%m-%d", end_datetime=None)
        fake_dueDate = fakegen.future_date(end_date="+30d", tzinfo=None)
        fake_shippingAddress = fakegen.address()
        fake_billingAddress = fakegen.address()
        fake_otherDetails = faker.generate_fake(schema)

        sale = OrderSale.objects.get_or_create(invoiceNumber = fake_invoiceNumber,
                                                name = fake_name,
                                                invoiceDate = fake_invoiceDate,
                                                shipmentDate = fake_shipmentDate,
                                                dueDate = fake_dueDate,
                                                shippingAddress = fake_shippingAddress,
                                                billingAddress = fake_billingAddress,
                                                otherDetails = fake_otherDetails)[0]

if __name__ == '__main__':
    print('populating...')
    populate(50000)
    print('populating done!')