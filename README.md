## Install django-tables2
pip install django-tables2

## Install django filters
pip install django-filter

## Install Faker (for fake data)
pip install Faker

## Install faker-schema (for fake json data)
pip install faker-schema

Add 'django_tables2' and 'djagno_filters to settings.py

Make models in models.py

Run manage.py makemigrations

Run manage.py migrate

Run python populate.py to populate the database with fake data

make a superuser 

Run python manage.py createsuperuser

Run python manage.py runserver
