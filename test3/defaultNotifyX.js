    <script type="text/javascript">
        // Ajax-Notification related callback urls.
        var updateNotificationUrl = "/notifications/api/update/";
        var markNotificationUrl = "/notifications/mark/";
        var markAllNotificationUrl = "/notifications/mark-all/";
        var deleteNotificationUrl = "/notifications/delete/";

        // Jquery class selectors.
        var nfListClassSelector = ".notifications";
        var nfClassSelector = ".notification";
        var nfBoxListClassSelector = ".notification-container";
        var nfBoxClassSelector = ".notification-box";

        var markNotificationSelector = ".mark-notification";
        var markAllNotificationSelector = ".mark-all-notifications";
        var deleteNotificationSelector = ".delete-notification";

        var readNotificationClass = "read";
        var unreadNotificationClass = "unread";

        var notificationUpdateTimeInterval = "15000";

        //functions to handle ajax success.
        var updateSuccess = function (response) {
    var notification_box = $(nfBoxListClassSelector);
    var notifications = response.notifications;
    $.each(notifications, function (i, notification) {
        notification_box.prepend(notification.html);
    });
};

        var markSuccess = function (response, notification) {
    //console.log(response);
    if (response.action == 'read') {
        var mkClass = readNotificationClass;
        var rmClass = unreadNotificationClass;
        var action = 'unread';
    } else {
        mkClass = unreadNotificationClass;
        rmClass = readNotificationClass;
        action = 'read';
    }
    // console.log(notification.closest(nfClassSelector));
    notification.closest(nfSelector).removeClass(rmClass).addClass(mkClass);
    notification.attr('data-mark-action', action);

    toggle_text = notification.attr('data-toggle-text') || 'Mark as ' + action;
    notification.attr('data-toggle-text', notification.html());
    notification.html(toggle_text);
};

        var markAllSuccess = function (response) {
    //console.log(response);
    // console.log(response.action);
    if (response.action == 'read') {
        var mkClass = readNotificationClass;
        var rmClass = unreadNotificationClass;
    } else {
        mkClass = unreadNotificationClass;
        rmClass = readNotificationClass;
    }
    // console.log(mkClass);
    // console.log(rmClass);
    $(nfSelector).removeClass(rmClass).addClass(mkClass);
};

        var deleteSuccess = function (response, notification) {
    //console.log(response);
    var $selected_notification = notification.closest(nfClassSelector);
    $selected_notification.fadeOut(300, function () {
        $(this).remove()
    });
};
    </script>
