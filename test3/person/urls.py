from django.urls import path, include
from django.conf.urls import url
from person import views

app_name = 'person'

urlpatterns = [
    path('', views.DisplayOrder, name='order_sale'),
    path('logout/', views.user_logout, name='logout'),
    path('user_login/', views.user_login, name='user_login'),
    path('register/', views.register, name='register'),
    # url(r'^notifications/', include('notify.urls', 'notifications')),
]