from django.contrib import admin
from person.models import OrderSale, User 

# Register your models here.
admin.site.register(User)
admin.site.register(OrderSale)