import django_tables2 as tables 
from .models import OrderSale


class OrderTable(tables.Table):
        
    class Meta:
        model = OrderSale
        template_name = 'django_tables2/semantic.html'
        fields = ['name','invoiceNumber','invoiceDate','shipmentDate','dueDate','shippingAddress','billingAddress','otherDetails','val','Extra']


