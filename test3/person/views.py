from django.shortcuts import render
from person.models import OrderSale, User 
from django.db import models
from django_tables2 import RequestConfig
from .tables import OrderTable
from .filters import SaleFilter
from django.db.models.functions import Extract, Substr, Cast
from django.contrib.postgres.fields.jsonb import KeyTextTransform
from django.contrib.auth import login, authenticate, logout
from person.forms import UserForm
from django.contrib.auth.decorators import login_required
from notify.signals import notify


def DisplayOrder(request):

    user = User.objects.get(id=1)
    # user = list(User.objects.all())
    print(user)

    if request.GET.get('notify'):
        print('notify clicked')
        print(request.user)
        notify.send(request.user, recipient=request.user, actor=request.user,
                verb='notified')#, nf_type='followed_by_one_user')

    queryset = OrderSale.objects.annotate(Extra= Substr('name',1,6),val=KeyTextTransform('order_name','otherDetails'),order_name=Cast('otherDetails', models.TextField()),)
    f = SaleFilter(request.GET, queryset=queryset)
    table = OrderTable(f.qs)
    RequestConfig(request, paginate={"per_page": 100, "page": 1}).configure(table)
    return render(request, 'order_sale.html', {'table': table, 'filter': f})




def user_login(request):

    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request,user)
                queryset = OrderSale.objects.annotate(Extra= Substr('name',1,6),val=KeyTextTransform('order_name','otherDetails'),order_name=Cast('otherDetails', models.TextField()),)
                f = SaleFilter(request.GET, queryset=queryset)
                table = OrderTable(f.qs)
                RequestConfig(request, paginate={"per_page": 100, "page": 1}).configure(table)
                return render(request, 'order_sale.html', {'table': table, 'filter': f})
                # return render(request,'order_sale.html')

            else:
                return HttpResponse('Account not active')
        else:
            print('Someone tried to login and failed!')
            print('username: {} and password: {}'.format(username,password))
            queryset = OrderSale.objects.annotate(Extra= Substr('name',1,6),val=KeyTextTransform('order_name','otherDetails'),order_name=Cast('otherDetails', models.TextField()),)
            f = SaleFilter(request.GET, queryset=queryset)
            table = OrderTable(f.qs)
            RequestConfig(request, paginate={"per_page": 100, "page": 1}).configure(table)
            return render(request, 'order_sale.html', {'table': table, 'filter': f})
            # return render(request,'order_sale.html')

    else:
        return render(request,'login.html',{})

@login_required
def user_logout(request):
    logout(request)
    queryset = OrderSale.objects.annotate(Extra= Substr('name',1,6),val=KeyTextTransform('order_name','otherDetails'),order_name=Cast('otherDetails', models.TextField()),)
    f = SaleFilter(request.GET, queryset=queryset)
    table = OrderTable(f.qs)
    RequestConfig(request, paginate={"per_page": 100, "page": 1}).configure(table)
    return render(request, 'order_sale.html', {'table': table, 'filter': f})
    # return render(request,'order_sale.html')

def register(request):

    registered = False

    if request.method == 'POST':
        user_form = UserForm(data=request.POST)

        if user_form.is_valid():

            user = user_form.save()
            user.set_password(user.password)
            user.save()

            registered = True
        else:
            print(user_form.errors)

    else:
        user_form = UserForm()

    return render(request,'register.html',
                            {'user_form':user_form,
                              'registered':registered})
