import django_filters
from .models import OrderSale

class SaleFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(label='Name', lookup_expr = 'icontains')
    invoiceNumber = django_filters.CharFilter(label='Invoice Number', lookup_expr = 'icontains')
    invoiceDate = django_filters.CharFilter(label='Invoice Date', lookup_expr = 'icontains')
    shipmentDate = django_filters.CharFilter(label='Shipment Date', lookup_expr = 'icontains')
    dueDate = django_filters.CharFilter(label='Due Date', lookup_expr = 'icontains')
    shippingAddress = django_filters.CharFilter(label='Shipping Address', lookup_expr = 'icontains')
    billingAddress = django_filters.CharFilter(label='Billing Address', lookup_expr = 'icontains')
    Extra = django_filters.CharFilter(label='Extra', method='extra',lookup_expr = 'icontains')
    val = django_filters.CharFilter(label='val',method='val_def', lookup_expr='icontains')
    otherDetails = django_filters.CharFilter(label='otherDetails',method='other_def', lookup_expr='icontians')

    def other_def(self, queryset, name, Value):
        return queryset.filter(order_name__contains=Value)
        # return queryset.filter(otherDetails__contians={'order_name':Value})

    def val_def(self, queryset, name, Value):
        return queryset.filter(val__icontains=Value)

    def extra(self, queryset, name, Value):
        return queryset.filter(Extra__icontains=Value)

    class Meta:
        model = OrderSale
        # fields = ['name','invoiceNumber','invoiceDate','shipmentDate','dueDate','shippingAddress','billingAddress','val','Extra']#'otherDetails',
        fields = []
       
       
