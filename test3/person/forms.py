from django import forms
from person.models  import User
from django.contrib.auth.models import User

class UserForm(forms.ModelForm):

    password = forms.CharField(widget= forms.PasswordInput())
    
    class Meta():
        model = User
        fields = ('username','email','password')

        widgets = {

        }