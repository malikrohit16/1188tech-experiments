from django.db import models
from django.contrib.postgres.fields import JSONField
from datetime import *
from django.contrib.auth.models import User


class User(models.Model):
    
    user = models.OneToOneField(User,on_delete=models.CASCADE,)
    
    def __str__(self):
        return self.user.username


class OrderSale(models.Model):
    name = models.CharField(max_length=50, null=True, blank=True)
    invoiceNumber = models.CharField(max_length=50,unique=True)
    invoiceDate = models.DateTimeField(default=datetime.now())
    shipmentDate = models.DateField(blank=True, null=True)
    dueDate = models.DateField(blank=True, null=True)
    shippingAddress=models.TextField(blank=True, null=True)
    billingAddress=models.TextField(blank=True, null=True) 
    otherDetails = JSONField(blank=True, null=True) 


    def __str__(self):
        return self.invoiceNumber